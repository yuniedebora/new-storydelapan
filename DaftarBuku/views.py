from django.shortcuts import render
from django.http import JsonResponse
import requests

# Create your views here.
def DaftarBukuViews(request):
    return render(request, 'appHtml/story8.html')

def get_keyWord(request, key):
	url = 'https://www.googleapis.com/books/v1/volumes?q=' + key
	response = requests.get(url)
	response_json = response.json()

	return JsonResponse(response_json)
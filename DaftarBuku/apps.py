from django.apps import AppConfig


class DaftarbukuConfig(AppConfig):
    name = 'DaftarBuku'

from django.test import TestCase, Client,LiveServerTestCase
from django.urls import resolve

from .views import DaftarBukuViews, get_keyWord

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


# Create your tests here.
class UnitTest (TestCase):
    def test_url_can_be_accessed(self) :
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_url_calls_DaftarBukuViews (self) :
        response = resolve('/')
        self.assertEqual(response.func, DaftarBukuViews)
    
    def test_url_calls_get_keyWord (self) :
        response = resolve('/KeyWord/<str:key>')
        self.assertEqual(response.func, get_keyWord)
    
    def test_url_use_story8html (self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'appHtml/story8.html')
    
    def test_html_file_contains_some_tag (self) :
        response = Client().get('')
        content = response.content.decode('utf8')
        self.assertIn('<script', content)
    
    def test_html_file_contain_searchBox_and_submitButton(self):
        response = Client().get('')
        content = response.content.decode('utf8')
        self.assertIn('<input', content)
        self.assertIn('text', content)
        self.assertIn('submit', content)

    def test_html_contains_jquery(self):
        response = Client().get('')
        content = response.content.decode('utf8')
        self.assertIn("https://code.jquery.com/ui/1.12.1/jquery-ui.js", content)

    def test_ada_table_hasil(self):
        response = Client().get('')
        content = response.content.decode('utf8')
        self.assertIn("<table", content)

class FunctionalTest (LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTest, self).tearDown()

    def test_input_search(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get(self.live_server_url)

        #cari elemen
        search = selenium.find_element_by_id('search')
        submit = selenium.find_element_by_id('submitButton')
        
        
        search.send_keys('Harry')
        submit.send_keys(Keys.RETURN)
